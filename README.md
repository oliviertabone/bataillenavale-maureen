# BatailleNavale

## Comment jouer

Sur Mac :
    - Si HomeBrew et Python3 ne sont pas déjà installés:
    - Installer Homebrew avec la ligne de commande suivante :
    - /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    - Toujours dans le terminal, taper brew install python
    - Enfin taper python3 --version pour voir la version (chez moi je suis en Python 3.9.4)
    - Placez vous dans le dossier, puis taper python3 main.py
    - Jouez :)

Sur Linux :
    - Ouvrez un terminal et tapez
    - sudo apt update
    - sudo apt install python3
    - Enfin taper python3 --version pour voir la version (chez moi je suis en Python 3.9.4)
    - Placez vous dans le dossier, puis taper python3 main.py
    - Jouez :)


## Les remarques

Pourquoi Python ?
    - Simple d'utilisation
    - Accéssible à tous
    - Possède beaucoup de fonctionnalités et permet énormément de choses
    - Il n'y a pas besoin de télécharger des dépendances pour pouvoir les utiliser, les imports suffisent
    - Facile à exécuter
    - Si j'avais voulu allez plus loin, j'aurais également pu utiliser les librairies que propose Python afin d'avoir une interface de jeu vidéo.


Sur ce jeu de bataille navale, le choix de la taille de la grille et des bateaux sont imposés, et les bateaux sont placés aléatoirement sur la grille.

Lorsque vous trouvé une partie de bateau, un 'X' apparait sur la grille.
Si vous manquez votre cible, un '-' apparait.
Le jeu s'arrête lorsque vous avez trouvé tous les bateaux.

en arrière, il y a deux grilles. La première contient le placement des bateaux, et la seconde, est la grille de jeu.
Ce choix a été mis en place afin d'afficher une grille de jeu vide, et pour faciliter la vérification.
Lorsque vous entrez une coordonnée, le programme va vérifier dans la grille de placement si un bateau existe à cet endroit. Si c'est le cas, il va afficher un X dans la grille de jeu, et transformer le 'B' (signe pour la présence d'un bateau) en 'X', ce qui va permettre d'arrêter le jeu lorsque tous les 'B' se seront transformés en 'X'.

En tentant de mettre en place la contrainte "Les navires ne peuvent pas se toucher (même en diagonale).", je me suis heurtée à une erreur dû à un soucis de dépassement, que je n'ai pas réussi à résoudre, même avec l'aide d'internet. Vous pouvez retrouver les lignes bloquantes en 31 et 41 dans le code. J'ai tenté différentes approches, mais aucunes ne fonctionnaient. Dans ce cas, pour résoudre mon problème, j'aurais demandé à une autre personne son avis. Cette dernière aurait peut être pu avoir une manière de penser différente de la mienne.
De ce fait, je n'ai pas pu mettre les 10 bateaux demandés, car cela serait devenu injouable. Je n'en ai donc placé que 7.


## Intégration continue et Tests

J'ai fais le choix de mettre de l'intégration continue, mais étant la première fois que j'en met en place, je ne sais pas trop si je l'ai bien utilisé, malgré l'aide donnée de GitLab. J'ai voulu en mettre justement pour essayer, et me tester dessus.

Il en va de même pour les tests, j'ai choisi d'en faire afin d'avoir un projet complet, et donc qui inclu les tests.

