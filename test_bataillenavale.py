import unittest

from bataillenavale import afficher_grille, placer_bateau

class TestBatailleNavale(unittest.TestCase):

    #Vérifie l'affichage de la grille de jeu
    def test_afficher_grille(self):
        grille = [["A"] * 15 for _ in range(15)]
        expected_output = (
            "  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15\n" +
            "1 A A A A A A A A A A A A A A A \n" +
            "2 A A A A A A A A A A A A A A A \n" +
            "3 A A A A A A A A A A A A A A A \n" +
            "4 A A A A A A A A A A A A A A A \n" +
            "5 A A A A A A A A A A A A A A A \n" +
            "6 A A A A A A A A A A A A A A A \n" +
            "7 A A A A A A A A A A A A A A A \n" +
            "8 A A A A A A A A A A A A A A A \n" +
            "9 A A A A A A A A A A A A A A A \n" +
            "10 A A A A A A A A A A A A A A A \n" +
            "11 A A A A A A A A A A A A A A A \n" +
            "12 A A A A A A A A A A A A A A A \n" +
            "13 A A A A A A A A A A A A A A A \n" +
            "14 A A A A A A A A A A A A A A A \n" +
            "15 A A A A A A A A A A A A A A A \n"
        )

        with self.subTest():
            self.assertEqual(afficher_grille(grille), expected_output)

    def test_placer_bateau_horizontal(self):
        grillebateaux = [[" " for _ in range(15)] for _ in range(15)]
        placer_bateau(grillebateaux, 4)

        # Vérifiez que le bateau a été correctement placé horizontalement
        for i in range(4):
            self.assertEqual(grillebateaux[0][i], 'B')

    def test_placer_bateau_vertical(self):
        grillebateaux = [[" " for _ in range(15)] for _ in range(15)]
        placer_bateau(grillebateaux, 4)

        # Vérifiez que le bateau a été correctement placé verticalement
        for i in range(4):
            self.assertEqual(grillebateaux[i][0], 'B')

if __name__ == '__main__':
    unittest.main()